var http = require('http');
var url = require('url');
var fs = require('fs');
var jsonObject = require('./data.json');
var port = 3000;
var regex =/(?:http(?:s)*:|www)\/\/(?:[a-z.0-9\/?=])+/ig;

//handler function to serve the user query
function handleTweets(parsed_url,response){
    if('id' in parsed_url.query){
      var json_data = getTweet(parsed_url.query['id']);
      response.writeHead(200,{'Content-Type': 'application/json'});
      response.end(JSON.stringify(json_data));
      console.log("Get the specific tweet id");
    }else {
      //get all the tweets
      var json_data = getAllTweets();
      response.writeHead(200,{'Content-Type': 'application/json'});
      response.end(JSON.stringify(json_data));
    }
}

function handleUsers(parsed_url,response){
  if('user' in parsed_url.query){
    var json_data = getUser(parsed_url.query['user']);
    response.writeHead(200,{'Content-Type': 'application/json'});
    response.end(JSON.stringify(json_data));
  }else {
    var json_data = getAllUsers();
    console.log(JSON.stringify(json_data));
    response.writeHead(200,{'Content-Type': 'application/json'});
    response.end(JSON.stringify(json_data));
  }
}

function handleUrls(response){
  var json_data = getAllUrls();
  response.writeHead(200,{'Content-Type': 'application/json'});
  response.end(JSON.stringify(json_data));
}

function getAllTweets(){
  //go through all the available tweets and get createtime,id,and tweet text
  var all_tweets = [];
  jsonObject.forEach(function(tweet, index,array){
    var jsonResponse = {};
    if("text" in tweet) jsonResponse.text = tweet['text'];
    if("id" in tweet) jsonResponse.id = tweet['id'];
    if("created_at" in tweet) jsonResponse.created_at = tweet['created_at'];
    if(Object.keys(jsonResponse).length > 0) all_tweets.push(jsonResponse);
  });
  return all_tweets;
}

function getAllUsers(){
  var users = new Map();
  jsonObject.forEach(function (tweet, index, array) {
    var user_info = {};
    if("user" in tweet) {
      var user = tweet['user'];
      if("name" in user) user_info.name = user['name'];
      if("id" in user) user_info.id = user['id'];
      if("description" in user) user_info.description = user['description'];
      if("screen_name" in user && !(users.has(user['screen_name']))){
        console.log("I am here");
        user_info.screen_name = user['screen_name'];
        users.set(user['screen_name'],user_info);
      }
    }
  });
  var users_list = [];
  for (user of users) {
      users_list.push(user[1]);
  }
  return users_list;
}

function getAllUrls(){
//enter url in a Map
//pop it out and enter into and array and assign it to a particular tweet
  var parsed_urls = {};
  var find_url = function(tweet,index,array){
    var urls = new Map();
    for (var field in tweet) {
     if (tweet.hasOwnProperty(field)) {
        if(tweet[field] !== null){
          var matched_str;
          while ((matched_str = regex.exec(JSON.stringify(tweet[field]))) !== null) {
            if(!urls.has(matched_str[0]))
              urls.set(matched_str[0],0);
              regex.lastIndex;
          }
        }
     }
   }
   //pop the keys in the map into an array and assign it to JSON data
   var url_iter = urls.keys();
   var list_urls = [];
   for(value of url_iter){
     list_urls.push(value);
   }
   parsed_urls[tweet['id']] = JSON.stringify(list_urls);
 };
 jsonObject.forEach(find_url);
 return parsed_urls;
}

function getTweet(id){
    var found_tweet = jsonObject.find(function (tweet,index,array) {
      if("id" in tweet){
        return (tweet['id'].toString() == id)
      }
      return false;
    });
    if(found_tweet === undefined)
    return null;
    var tweet_info = {};
    var user = found_tweet['user'];
    tweet_info['text'] = found_tweet['text'];
    tweet_info['name'] = user['name'];
    tweet_info['screen_name'] = user['screen_name'];
    tweet_info['retweet_count'] = found_tweet['retweet_count'];
    return tweet_info;
}

function getUser(screen_name){
  var found_tweet = jsonObject.find(function (tweet,index,array) {
    if('user' in tweet){
      var user = tweet['user'];
      if(user['screen_name'] == screen_name){
        return true;
      }
    }
    return false;
  });

  if(found_tweet === undefined)
    return null;

  var found_user = found_tweet['user'];
  var user_info = {};
  //add the needed details here
  if(found_user !== undefined){
    user_info['screen_name'] = found_user['screen_name'];
    user_info['name'] = found_user['name'];
    user_info['location'] = found_user['location'];
    user_info['description'] = found_user['description'];
    user_info['friends_count'] = found_user['friends_count'];
    user_info['followers_count'] = found_user['followers_count'];
  }
  return user_info;
}

//function to serve requested files
function displayIndex(file_name,response){
    var offset = file_name.lastIndexOf('.');
    console.log(file_name,offset);
    var type = offset == -1 ? 'text/plain' : {html: 'text/html', css: 'text/css', js: 'text/js'}[file_name.substr(offset+1)];
    response.writeHead(200,{'Content-Type': type});
    fs.readFile(file_name,function (err,data) {
      if(err) throw err;
      response.end(data);
    });
}

function onCreate(req,res){
    var parsed_url = url.parse(req.url,true);
    console.log(parsed_url.pathname);
    if(parsed_url.pathname == '/')
      displayIndex('./index.html',res);
    else if(parsed_url.pathname == '/style.css')
      displayIndex('./style.css',res);
    else if(parsed_url.pathname == '/client.js')
      displayIndex('./client.js',res);
    else if(parsed_url.pathname == '/tweets')
      handleTweets(parsed_url,res);
    else if(parsed_url.pathname == '/users')
      handleUsers(parsed_url,res);
    else if(parsed_url.pathname == '/urls')
      handleUrls(res);
}

var server = http.createServer(onCreate);
server.listen(port,function(){console.log('server listening on port' + port);});
