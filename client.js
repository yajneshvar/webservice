
//AJAX query for all tweets
function displayAllTweets(){
  $.getJSON('/tweets',function(data){
    $.each(data,function(key,val) {
      $('#all-tweets').append("<div> <ul>" +
      "<li>" + "Tweet Id: " + val['id'] + "</li> <li>" + "Created at: " + val['created_at'] +
      "</li> <p> " + "Tweet :" + val['text'] +
      "</p> </ul> </div>");
    });
  });
}

//AJAX query to display all urls
function displayAllUrls(){
  $.getJSON('/urls',function (data) {
    $.each(data,function(key,val){
      $('#all-urls').append("<div> <ul>" +
      "<li>" + "Tweet Id:" + key + "</li>" +
      "</ul> </div>");
      var url_list = JSON.parse(val);
      //for each url need to insert it
      url_list.forEach(function (url,index,array) {
        var current_list = $('#all-urls > div:last > ul');
        current_list.append("<li>" + url + "</li>");
      });
    });
  });
}


//AJAX query to get all users
function displayAllUsers(){
  $.getJSON('/users',function (data) {
    console.log(data);
    data.forEach(function (user_info,index,array) {
      $('#all-users').append("<div><ul>" + "<li>" + "Screen name: " + user_info.screen_name + "</li></ul></div>");
        for (var keys in user_info) {
          if (user_info.hasOwnProperty(keys)) {
              if(keys != 'screen_name'){
                $('#all-users > div:last > ul').append("<li>" + keys +":  " + user_info[keys] + "</li>");
              }
          }
        }
    });
  });

}


//AJAX query to get specific user
function displaySpecificUser(){
  var user_name = $("#user_name").val();
  console.log(user_name);
  $('#get-user > ul').replaceWith("<ul></ul>");
  //compose the query string to get the specific user name
  $.getJSON('/users?user='+user_name,function (data) {
    if(data === null)
      $('#get-user > ul').append("<a> User not found </a>");
    for (var keys in data) {
      if (data.hasOwnProperty(keys)) {
        $('#get-user > ul').append("<li>" + keys + ": " + data[keys] + "</li>");
      }
    }
  });
}

//AJAX query to get specific tweet
function displaySpecificTweet(){
  var tweet_id = $("#tweet_id").val();
  console.log(user_name);
  $('#get-tweet > ul').replaceWith("<ul></ul>");
  //compose the query string to get the specific user name
  $.getJSON('/tweets?id='+tweet_id,function (data) {
    if(data === null)
      $('#get-tweet > ul').append("<a> Tweet not found </a>");
    for (var keys in data) {
      if (data.hasOwnProperty(keys)) {
        $('#get-tweet > ul').append("<li>" + keys + ": " + data[keys] + "</li>");
      }
    }
  });
}

$(window).load(function () {
  displayAllTweets();
  displayAllUsers();
  displayAllUrls();
$("#getUser").click(displaySpecificUser);
$("#getTweet").click(displaySpecificTweet);
});
